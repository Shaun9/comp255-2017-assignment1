package org.bitbucket.sudoku;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import org.bitbucket.sudoku.Sudoku;

/**
 * Simple tests for checking whether a int is a suqare
 */
@RunWith( Parameterized.class )
public class SquareIntTests {

  /**
   *  The collection of tests as an Array of (int x expected result)
   */
  /* *INDENT-OFF* */
   @Parameters(name = "{index}: int {0} {2}")
   public static Collection<Object[]> data() {
     // testcases wil de displayed as test[0], test[1] and so on
     return Arrays.asList(new Object[][] {
       { 0  , true  , "is a square"}     ,
       { 1  , true  , "is a square"}     ,
       { 2  , false , "is not a square"} ,
       { 3  , false , "is not a square"} ,
       { 4  , true  , "is a square"}     ,
       { 5  , false , "is not a square"} ,
       { 6  , false , "is not a square"} ,
       { 9  , true  , "is a square"}     ,
       { 10 , false , "is not a square"} ,
       { 13 , false , "is not a square"} ,
       { 16 , true  , "is a square"}     ,
       { 21 , false , "is not a square"} ,
       { 25 , true  , "is a square"}
     });
   }
  /* *INDENT-ON* */

  // Test parameters
  // @link{https://github.com/junit-team/junit4/wiki/Parameterized-tests}
  private int k;
  private boolean squareStatus;
  private String information;

  /**
   *  Constructor for the tests
   */
  public SquareIntTests(int input, boolean expectedSquareStatus, String reason) {
    k = input;
    squareStatus = expectedSquareStatus;
    information = reason;
  }

  /**
   * Run all the tests
   */
  @Test
  public void test()  {
    assertEquals(squareStatus, new Sudoku (new int[][] {}).isSquare( k));
  }
}
